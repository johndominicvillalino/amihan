class Letter {
    constructor(letter) {
        this.letter = letter
    }

    draw(j) {
        let l = ''
        const set = new Set([1, 8])
        switch (this.letter) {
            case 'X':
                for (let i = 1; i <= Math.trunc(j / 2); i++) {
                    for (let x = 1; x <= Math.trunc(j / 2); x++) {
                        if (x - i == 0) {
                            l += `*`
                        }
                        l += ' '
                    }
                    for (let x = Math.trunc(j / 2); x >= 1; x--) {
                        if (x - i == 0) {
                            l += `*`
                        }
                        l += ' '
                    }
                    l += '\n'
                }
                if (j % 2 != 0) {
                    for (let s = 1; s <= j; s++) {
                        if (s == Math.trunc(j / 2) + 1) {
                            l += '*'
                        } else {
                            l += ' '
                        }
                    }
                    l += '\n'
                }
                for (let i = Math.trunc(j / 2); i >= 1; i--) {
                    for (let x = 1; x <= Math.trunc(j / 2); x++) {
                        if (i - x == 0) {
                            l += `*`
                        }
                        l += ' '
                    }
                    for (let x = Math.trunc(j / 2); x >= 1; x--) {
                        if (i - x == 0) {
                            l += `*`
                        }
                        l += ' '
                    }
                    l += '\n'
                }
                return l
                break;

            case 'O':
                for (let i = 1; i <= j; i++) {
                    if (i == 1 || i == j) {
                        for (let x = 1; x <= 8; x++) {
                            if (set.has(x)) {
                                l += ' '
                            } else {
                                l += '*'
                            }
                        }
                        l += '\n'
                    } else {
                        l += '*'
                        let space = ' '.repeat(8 - 2)
                        l += space
                        l += '*\n'
                    }
                }
                return l
                break;

            case 'Y':
                for (let i = 1; i <= Math.trunc(j / 2); i++) {
                    for (let x = 1; x <= Math.trunc(j / 2); x++) {
                        if (x - i == 0) {
                            l += `*`
                        }
                        l += ' '
                    }
                    for (let x = Math.trunc(j / 2); x >= 1; x--) {
                        if (x - i == 0) {
                            l += `*`
                        }
                        l += ' '
                    }
                    l += '\n'
                }
                if (j % 2 != 0) {
                    for (let s = 1; s <= j; s++) {
                        if (s == Math.trunc(j / 2) + 1) {
                            l += '*'
                        } else {
                            l += ' '
                        }
                    }
                    l += '\n'
                }

                for (let i = Math.trunc(j / 2); i >= 1; i--) {
                    for (let x = 1; x <= Math.trunc(j / 2); x++) {
                        if (i - x == 0) {
                            l += `*`
                        }
                        l += ' '
                    }
                    l += '\n'
                }
                return l;
                break;
            case 'Z':
                for (let i = 1; i <= j; i++) {
                    if (i == 1) {
                        l += '*'.repeat(j)
                        l += '\n'
                    }
                    for (let x = j; x >= 0; x--) {
                        if (!set.has(i)) {
                            if (x - i == 0) {
                                l += `*`
                            }
                            l += ' '
                        }
                    }
                    !set.has(i) ? l += '\n' : ''

                    if (i == j-1) {
                        l += '*'.repeat(j-1)
                    }
                }
                return l;
                break;
                default:
                    return "Please select only [O, X, Y, Z] - When you're ready, re run app.js"
        }
    }
}


module.exports = Letter