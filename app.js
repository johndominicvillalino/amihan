const Letter = require('./Letters')

const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

rl.question("Choose a letter ( X, Y, O, Z ) ", letter => {
    const letters = new Set(['X', 'Y', 'O', 'Z'])
    if (!letters.has(letter.toUpperCase())) {
        console.log('Please choose within the selection only. Re-run app.js to start again')
        rl.close()
        return
    }

    rl.question('\nIndicate the size in number and should be more than 3 - ', size => {
  
        if (parseInt(size) <= 3 || !parseInt(size)) {
            console.log('\nPlease make sure the size is a number and more than 3. Re-run app.js to start again')
            rl.close()
            return
        }
        const s = new Letter(letter)
        console.log('\n' + s.draw(size))
        rl.close()

    });


}
);






